package com.cuicui.demo.entity;

import java.io.Serializable;
import java.util.Date;

public class DepartmentData implements Serializable {
    private Integer id;

    private String name;

    private Integer parentId;

    private String desc;

    private Date createTime;

    private Integer isdealted;

    private static final long serialVersionUID = 1L;

    public DepartmentData(Integer id, String name, Integer parentId, String desc, Date createTime, Integer isdealted) {
        this.id = id;
        this.name = name;
        this.parentId = parentId;
        this.desc = desc;
        this.createTime = createTime;
        this.isdealted = isdealted;
    }

    public DepartmentData() {
        super();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public Integer getParentId() {
        return parentId;
    }

    public void setParentId(Integer parentId) {
        this.parentId = parentId;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc == null ? null : desc.trim();
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Integer getIsdealted() {
        return isdealted;
    }

    public void setIsdealted(Integer isdealted) {
        this.isdealted = isdealted;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", name=").append(name);
        sb.append(", parentId=").append(parentId);
        sb.append(", desc=").append(desc);
        sb.append(", createTime=").append(createTime);
        sb.append(", isdealted=").append(isdealted);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}