package com.cuicui.demo.entity;

import java.io.Serializable;
import java.util.Date;

public class LogData implements Serializable {
    private Integer id;

    private String createName;

    private String userAgent;

    private String content;

    private String requestedurl;

    private String ip;

    private Date createTime;

    private static final long serialVersionUID = 1L;

    public LogData(Integer id, String createName, String userAgent, String content, String requestedurl, String ip, Date createTime) {
        this.id = id;
        this.createName = createName;
        this.userAgent = userAgent;
        this.content = content;
        this.requestedurl = requestedurl;
        this.ip = ip;
        this.createTime = createTime;
    }

    public LogData() {
        super();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCreateName() {
        return createName;
    }

    public void setCreateName(String createName) {
        this.createName = createName == null ? null : createName.trim();
    }

    public String getUserAgent() {
        return userAgent;
    }

    public void setUserAgent(String userAgent) {
        this.userAgent = userAgent == null ? null : userAgent.trim();
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content == null ? null : content.trim();
    }

    public String getRequestedurl() {
        return requestedurl;
    }

    public void setRequestedurl(String requestedurl) {
        this.requestedurl = requestedurl == null ? null : requestedurl.trim();
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip == null ? null : ip.trim();
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", createName=").append(createName);
        sb.append(", userAgent=").append(userAgent);
        sb.append(", content=").append(content);
        sb.append(", requestedurl=").append(requestedurl);
        sb.append(", ip=").append(ip);
        sb.append(", createTime=").append(createTime);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}