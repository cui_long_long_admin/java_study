package com.cuicui.demo.entity;

import java.io.Serializable;
import java.util.Date;

public class UserData implements Serializable {
    private Integer id;

    private String name;

    private String phone;

    private Integer gender;

    private String pwd;

    private Integer isDeleted;

    private Date createTime;

    private Integer avatarid;

    private Integer departmentId;

    private static final long serialVersionUID = 1L;

    public UserData(Integer id, String name, String phone, Integer gender, String pwd, Integer isDeleted, Date createTime, Integer avatarid, Integer departmentId) {
        this.id = id;
        this.name = name;
        this.phone = phone;
        this.gender = gender;
        this.pwd = pwd;
        this.isDeleted = isDeleted;
        this.createTime = createTime;
        this.avatarid = avatarid;
        this.departmentId = departmentId;
    }

    public UserData() {
        super();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone == null ? null : phone.trim();
    }

    public Integer getGender() {
        return gender;
    }

    public void setGender(Integer gender) {
        this.gender = gender;
    }

    public String getPwd() {
        return pwd;
    }

    public void setPwd(String pwd) {
        this.pwd = pwd == null ? null : pwd.trim();
    }

    public Integer getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(Integer isDeleted) {
        this.isDeleted = isDeleted;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Integer getAvatarid() {
        return avatarid;
    }

    public void setAvatarid(Integer avatarid) {
        this.avatarid = avatarid;
    }

    public Integer getDepartmentId() {
        return departmentId;
    }

    public void setDepartmentId(Integer departmentId) {
        this.departmentId = departmentId;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", name=").append(name);
        sb.append(", phone=").append(phone);
        sb.append(", gender=").append(gender);
        sb.append(", pwd=").append(pwd);
        sb.append(", isDeleted=").append(isDeleted);
        sb.append(", createTime=").append(createTime);
        sb.append(", avatarid=").append(avatarid);
        sb.append(", departmentId=").append(departmentId);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}