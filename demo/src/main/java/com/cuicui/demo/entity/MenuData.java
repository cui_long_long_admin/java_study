package com.cuicui.demo.entity;

import java.io.Serializable;

public class MenuData implements Serializable {
    private Integer id;

    private String name;

    private Integer type;

    private String path;

    private Integer needpermission;

    private String icon;

    private static final long serialVersionUID = 1L;

    public MenuData(Integer id, String name, Integer type, String path, Integer needpermission, String icon) {
        this.id = id;
        this.name = name;
        this.type = type;
        this.path = path;
        this.needpermission = needpermission;
        this.icon = icon;
    }

    public MenuData() {
        super();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path == null ? null : path.trim();
    }

    public Integer getNeedpermission() {
        return needpermission;
    }

    public void setNeedpermission(Integer needpermission) {
        this.needpermission = needpermission;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon == null ? null : icon.trim();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", name=").append(name);
        sb.append(", type=").append(type);
        sb.append(", path=").append(path);
        sb.append(", needpermission=").append(needpermission);
        sb.append(", icon=").append(icon);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}