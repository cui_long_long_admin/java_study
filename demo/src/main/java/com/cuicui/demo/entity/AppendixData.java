package com.cuicui.demo.entity;

import java.io.Serializable;
import java.util.Date;

public class AppendixData implements Serializable {
    private Integer id;

    private String name;

    private String path;

    private Date createTime;

    private String type;

    private Integer size;

    private static final long serialVersionUID = 1L;

    public AppendixData(Integer id, String name, String path, Date createTime, String type, Integer size) {
        this.id = id;
        this.name = name;
        this.path = path;
        this.createTime = createTime;
        this.type = type;
        this.size = size;
    }

    public AppendixData() {
        super();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path == null ? null : path.trim();
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type == null ? null : type.trim();
    }

    public Integer getSize() {
        return size;
    }

    public void setSize(Integer size) {
        this.size = size;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", name=").append(name);
        sb.append(", path=").append(path);
        sb.append(", createTime=").append(createTime);
        sb.append(", type=").append(type);
        sb.append(", size=").append(size);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}