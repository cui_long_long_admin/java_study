package com.cuicui.demo.entity;

import java.io.Serializable;

public class DictionaryValueData implements Serializable {
    private Integer id;

    private String name;

    private String value;

    private Integer dictionaryId;

    private String remark;

    private static final long serialVersionUID = 1L;

    public DictionaryValueData(Integer id, String name, String value, Integer dictionaryId, String remark) {
        this.id = id;
        this.name = name;
        this.value = value;
        this.dictionaryId = dictionaryId;
        this.remark = remark;
    }

    public DictionaryValueData() {
        super();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value == null ? null : value.trim();
    }

    public Integer getDictionaryId() {
        return dictionaryId;
    }

    public void setDictionaryId(Integer dictionaryId) {
        this.dictionaryId = dictionaryId;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark == null ? null : remark.trim();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", name=").append(name);
        sb.append(", value=").append(value);
        sb.append(", dictionaryId=").append(dictionaryId);
        sb.append(", remark=").append(remark);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}