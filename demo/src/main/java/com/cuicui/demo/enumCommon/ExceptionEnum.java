package com.cuicui.demo.enumCommon;

public enum ExceptionEnum implements BaseErrorInfo {
    SUCCESS("200", "请求成功!"),
    PARAMS_ERROR("400", "参数异常!"),
    SYSTEM_ERROR("500", "系统繁忙");

    private final String errorCode;

    private final String errorMessage;

    ExceptionEnum(String errorCode, String errorMessage ){
        this.errorCode = errorCode;
        this.errorMessage = errorMessage;
    }

    @Override
    public String getErrorCode() {
        return errorCode;
    }

    @Override
    public String getErrorMessage() {
        return errorMessage;
    }
}
