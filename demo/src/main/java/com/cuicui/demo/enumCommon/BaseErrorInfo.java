package com.cuicui.demo.enumCommon;

public interface BaseErrorInfo {
    String getErrorCode();

    String getErrorMessage();
}
