package com.cuicui.demo.controller;

import com.cuicui.demo.service.AppendixService;
import com.cuicui.demo.utils.CommonResponse;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

@RestController
@CrossOrigin
@Tag(name="附件")
@RequestMapping("/file")
public class AppendixController {

    @Autowired
    private AppendixService appendixService;

    @PostMapping(value = "/upload", headers="content-type=multipart/form-data")
    @Operation(summary = "附件上传")
    public CommonResponse upload (@RequestParam MultipartFile file) {
       return CommonResponse.success(appendixService.upload(file));
    }
}
