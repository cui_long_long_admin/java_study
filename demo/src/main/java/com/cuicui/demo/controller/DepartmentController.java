package com.cuicui.demo.controller;

import com.cuicui.demo.service.DepartmentService;
import com.cuicui.demo.utils.CommonResponse;
import com.cuicui.demo.vo.DepartmentVO;
import com.github.pagehelper.PageInfo;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("/department")
@Tag(name = "部门")
public class DepartmentController {
    @Autowired
    private DepartmentService departmentService;


    /**
     * @param String name
     * @return List<DepartmentVO>
     */
    @GetMapping("/getList")
    @Operation(summary = "部门列表")
    public CommonResponse<List<DepartmentVO>> getList(@RequestParam(required = false) String name) {
        return CommonResponse.success(departmentService.getList(name));
    }

    @GetMapping("/getPage")
    @Operation(summary = "部门分页")
    public CommonResponse<PageInfo<DepartmentVO>> getPage(
            @RequestParam(required = false) String name,
            @RequestParam(required = true, defaultValue = "1") Integer pageNo,
            @RequestParam(required = true, defaultValue = "20") Integer  pageSize
    ) {
        return CommonResponse.success(departmentService.getPage(name, pageNo, pageSize));
    }

    /**
     *
     * @return
     */
    @PostMapping("/add")
    @Operation(summary = "部门新增")
    public CommonResponse<Integer> addDepartment (@RequestBody DepartmentVO departmentVO) {
        return CommonResponse.success(departmentService.addDepartment(departmentVO));
    }

    /**
     *
     * @param id
     * @return
     */
    @DeleteMapping("/remove")
    @Operation(summary = "部门删除")
    public CommonResponse<Boolean> remove (@RequestBody Integer id) {
        return CommonResponse.success(departmentService.remove(id));
    }

    /**
     *
     * @param departmentVO
     * @return
     */
    @PutMapping("/update")
    @Operation(summary = "部门修改")
    public CommonResponse<Boolean> update(@RequestBody DepartmentVO departmentVO) {
        return CommonResponse.success(departmentService.update(departmentVO));
    }
}
