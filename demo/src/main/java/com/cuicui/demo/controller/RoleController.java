package com.cuicui.demo.controller;

import com.cuicui.demo.service.RoleService;
import com.cuicui.demo.utils.CommonResponse;
import com.cuicui.demo.vo.RoleVO;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
@RequestMapping("/role")
@Tag(name = "角色")
public class RoleController {

    @Autowired
    private RoleService roleService;

    /**
     * 获取角色列表
     * @param name 角色名称
     * @return
     */
    @GetMapping("/getlist")
    @Operation(summary = "角色列表")
    public CommonResponse getList (@RequestParam(defaultValue = "", required = false) String name) {
        return roleService.getList(name);
    }

    /**
     * 分页查询角色列表
     * @return
     */
    @GetMapping("/getPage")
    @Operation(summary = "角色分页")
    public CommonResponse getPage (@RequestParam(required = false) String name,
                                  @RequestParam(required = false, defaultValue = "1") Integer pageNo,
                                  @RequestParam(required = false, defaultValue = "20") Integer pageSize) {
        return roleService.getPage(name, pageNo, pageSize);
    }

    /**
     *
     * @param roleVO
     * @return
     */
    @PostMapping("/add")
    @Operation(summary = "新增角色")
    public CommonResponse add (@RequestBody RoleVO roleVO) {
        return roleService.add(roleVO);
    }


    /**
     *
     * @param id
     * @return
     */
    @DeleteMapping("/remove")
    @Operation(summary = "删除角色")
    public CommonResponse remove (@RequestBody(required = true) Integer id) {
        return roleService.remove(id);
    }


    /**
     *
     * @param roleVO
     * @return
     */
    @PutMapping("/update")
    @Operation(summary = "修改角色")
    public CommonResponse update (@RequestBody RoleVO roleVO) {
        return roleService.update(roleVO);
    }
}
