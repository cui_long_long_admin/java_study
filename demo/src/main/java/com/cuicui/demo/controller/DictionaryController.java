package com.cuicui.demo.controller;

import com.cuicui.demo.service.DictionaryService;
import com.cuicui.demo.service.DictionaryValueService;
import com.cuicui.demo.utils.CommonResponse;
import com.cuicui.demo.vo.DictionaryVO;
import com.cuicui.demo.vo.DictionaryValueVO;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
@RequestMapping("/dictionary")
@Tag(name = "字典管理")
public class DictionaryController {
    @Autowired
    private DictionaryService dictionaryService;

    @Autowired
    private DictionaryValueService dictionaryValueService;

    /**
     *
     * @return
     */
    @GetMapping("/getList")
    @Operation(summary = "字典列表")
    public CommonResponse<DictionaryVO> getList () {
        return dictionaryService.getList();
    }

    /**
     * @param pageNo
     * @param pageSize
     * @param name
     * @return
     */
    @GetMapping("/getPage")
    @Operation(summary = "字典分页")
    public CommonResponse getPage (
            @RequestParam(required = true, defaultValue = "1") Integer pageNo,
            @RequestParam(required = true, defaultValue = "20") Integer pageSize,
            @RequestParam(required = false) String name
            ){
        return dictionaryService.getPage(name, pageNo, pageSize);
    }

    /**
     * @return
     */
    @GetMapping("/getValueList")
    @Operation(summary = "字典值列表")
    public CommonResponse getDictionaryValue (
            @RequestParam(required = false) Integer id,
            @RequestParam(required = false) String code
    ){
        return dictionaryValueService.getDictionaryValue(id, code);
    }

    /**
     * @param dictionaryVO
     * @return
     */
    @PostMapping("/add")
    @Operation(summary = "字典新增")
    public CommonResponse add (@RequestBody DictionaryVO dictionaryVO) {
        return dictionaryService.add(dictionaryVO);
    }

    /**
     *
     * @param dictionaryValueVO
     * @return
     */
    @PostMapping("/addValue")
    @Operation(summary = "字典值新增")
    public CommonResponse addValue (@RequestBody DictionaryValueVO dictionaryValueVO){
        return dictionaryValueService.addValue(dictionaryValueVO);
    }

    /**
     *
     * @param id
     * @return
     */
    @DeleteMapping("/remove")
    @Operation(summary = "删除字典")
    public CommonResponse remove (@RequestBody Integer id) {
        return dictionaryService.remove(id);
    }

    /**
     *
     * @param id
     * @return
     */
    @DeleteMapping("/removeValue")
    @Operation(summary = "删除字典值")
    public CommonResponse removeValue (@RequestBody Integer id) {
        return dictionaryService.removeValue(id);
    }

    /**
     * 更新字典信息
     *
     * @param dictionaryVO 包含更新信息的字典对象
     * @return 更新操作的结果
     */
    @PutMapping("/update")
    @Operation(summary = "修改字典")
    public CommonResponse update (@RequestBody DictionaryVO dictionaryVO){
        return dictionaryService.update(dictionaryVO);
    }

    /**
     * 更新字典值信息
     *
     * @param dictionaryValueVO 包含更新信息的字典对象值
     * @return 更新操作的结果
     */
    @PutMapping("/updateValue")
    @Operation(summary = "修改字典")
    public CommonResponse updateValue (@RequestBody DictionaryValueVO dictionaryValueVO){
        return dictionaryService.updateValue(dictionaryValueVO);
    }
}
