package com.cuicui.demo.controller;

import com.cuicui.demo.annotation.LogRecorder;
import com.cuicui.demo.service.UserDataService;
import com.cuicui.demo.utils.CommonResponse;
import com.cuicui.demo.vo.UserVO;
import com.github.pagehelper.PageInfo;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
@RequestMapping("/user")
@Tag(name = "用户管理")
public class UserDataController {

    @Autowired
    private UserDataService userService;

    /**
     * @param name     用户名
     * @param pageNo   页数
     * @param pageSize 单页数量
     * @return CommonResponse<PageInfo < UserVO>> 分页的用户列表
     */
    @GetMapping("/userPage")
    @Operation(summary = "用户分页")
    @LogRecorder(operation = "用户分页查询")
    public CommonResponse<PageInfo<UserVO>> getPage(
            @RequestParam(required = false) Integer departmentId,
            @RequestParam(required = false, defaultValue = "") String name,
            @RequestParam(required = false, defaultValue = "1") Integer pageNo,
            @RequestParam(required = false, defaultValue = "20") Integer pageSize) {
        return CommonResponse.success(userService.getPage(name, pageNo, pageSize, departmentId));
    }

    /**
     * @param AddUserParams 用户信息
     * @return 用户Id
     */
    @PostMapping("/userAdd")
    @Operation(summary = "用户新增")
    public CommonResponse userAdd(@RequestBody @Valid UserVO AddUserParams) {
        Integer id = userService.Add(AddUserParams);
        if (id == -1) {
            return CommonResponse.front_error("当前用户已存在!");
        }
        return CommonResponse.success(id);
    }

    /**
     * @param id 用户主键
     * @return 删除标识
     */
    @DeleteMapping("/userRemove")
    @Operation(summary = "用户永久删除")
    public CommonResponse<Boolean> userRemove(@RequestBody(required = true) Integer id) {
        return CommonResponse.success(userService.remove(id));
    }

    @DeleteMapping("/userDelete")
    @Operation(summary = "用户假删除")
    public CommonResponse<Boolean> userDelete(@RequestBody(required = true) Integer id) {
        return CommonResponse.success(userService.del(id));
    }

    @PutMapping("/userUpdate")
    @Operation(summary = "用户信息修改")
    public CommonResponse<Boolean> userUpdate(@RequestBody(required = true) UserVO info) {
        return CommonResponse.success(userService.update(info));
    }

    @GetMapping("/getData")
    @Operation(summary = "根据token获取用户信息")
    public CommonResponse<UserVO> getData(HttpServletRequest request) {
        String token = request.getHeader("Authorization");
        return CommonResponse.success(userService.getData(token));
    }

    @GetMapping("/getDetail")
    @Operation(summary = "用户详情")
    public CommonResponse getDetail (@RequestParam Integer id) {
        return CommonResponse.success(userService.getDetail(id));
    }
}
