package com.cuicui.demo.controller;

import com.cuicui.demo.service.AuthService;
import com.cuicui.demo.utils.CommonResponse;
import com.cuicui.demo.vo.LoginVO;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
@RequestMapping("/auth")
@Tag(name="认证")
public class AuthController {

    @Autowired
    private AuthService authService;

    /**
     *
     * @param loginVO
     * @return  CommonResponse<String>
     */
    @PostMapping("/login")
    @Operation(summary = "用户登录")
    public CommonResponse<String> login (@RequestBody @Valid LoginVO loginVO) {
       return authService.login(loginVO);
    }
}
