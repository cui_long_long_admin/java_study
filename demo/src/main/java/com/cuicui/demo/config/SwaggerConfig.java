package com.cuicui.demo.config;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.enums.SecuritySchemeIn;
import io.swagger.v3.oas.annotations.enums.SecuritySchemeType;
import io.swagger.v3.oas.annotations.info.Info;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.security.SecurityScheme;
import org.springframework.stereotype.Component;

import static com.cuicui.demo.config.SwaggerConfig.TOKEN_HEADER;

@OpenAPIDefinition(
        info = @Info(
                title = "CuiCuiAPI文档",
                description = "CuiCui java后端描述",
                version = "v1.0.1"
        ),
        security = @SecurityRequirement(name = TOKEN_HEADER)
)
@SecurityScheme(
        type = SecuritySchemeType.APIKEY,
        name = TOKEN_HEADER,
        in = SecuritySchemeIn.HEADER)
@Component
public class SwaggerConfig {
    public static final String TOKEN_HEADER = "Authorization";
}
