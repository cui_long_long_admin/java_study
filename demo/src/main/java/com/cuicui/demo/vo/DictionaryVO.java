package com.cuicui.demo.vo;

import lombok.Data;

@Data
public class DictionaryVO {
    /**
     * 主键
     */
    private Integer id;

    /**
     * 姓名
     */
    private String name;

    /**
     * 编码
     */
    private String code;
}
