package com.cuicui.demo.vo;

import lombok.Data;

@Data
public class DictionaryValueVO {
    /**
     * 主键
     */
    private Integer id;

    /**
     * 名成
     */
    private String name;

    /**
     * 编码
     */
    private String value;

    /**
     * 字典id
     */
    private Integer dictionaryId;

    /**
     * 备注
     */
    private String remark;
}
