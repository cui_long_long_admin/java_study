package com.cuicui.demo.vo;

import lombok.Data;

@Data
public class RoleVO {

    /**
     * 主键
     */
    private Integer id;

    /**
     * 姓名
     */
    private String name;

    /**
     * 备注
     */
    private String desc;
}
