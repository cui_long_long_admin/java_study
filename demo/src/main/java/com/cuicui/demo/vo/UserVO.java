package com.cuicui.demo.vo;

import com.cuicui.demo.entity.UserData;
import org.hibernate.validator.constraints.Length;
import org.springframework.beans.BeanUtils;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.io.Serializable;
import java.util.Date;

@Data
@NoArgsConstructor
public class UserVO implements Serializable {
    /**
     * 用户主键
     */
    private Integer id;

    /**
     * 用户名
     */
    @Length(max = 32, message = "用户名不能超过32位")
    private String name;


    /**
     * 性别
     */
    private Integer gender;

    /**
     * 日期
     */
    private Date createTime;

    /**
     * 密码
     */
    private String pwd;

    /**
     * 电话
     */
    private String phone;

    /**
     * 删除标识
     */
    private Integer isDeleted;

    /**
     *
     * 部门id
     */
    private Integer departmentId;


    public UserVO(UserData user) {
        BeanUtils.copyProperties(user, this);
    }
}
