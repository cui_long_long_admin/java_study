package com.cuicui.demo.vo;

import com.cuicui.demo.entity.DepartmentData;
import com.cuicui.demo.entity.UserData;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.BeanUtils;

import java.util.Date;
@Data
@NoArgsConstructor
public class DepartmentVO {
    /**
     * 主键
     */
    private Integer id;

    /**
     * 部门名成
     */
    private String name;

    /**
     * 上级部门Id
     */
    private Integer parentId;

    /**
     * 部门描述
     */
    private String desc;

    /**
     * 创建时间
     */
    private Date createTime;


    public DepartmentVO(DepartmentData departmentData) {
        BeanUtils.copyProperties(departmentData, this);
    }
}
