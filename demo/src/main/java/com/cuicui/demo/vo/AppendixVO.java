package com.cuicui.demo.vo;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@NoArgsConstructor
public class AppendixVO {
    /**
     * 文件路径
     */
    String path;

    /**
     * 文件名成
     */
    String name;

    /**
     * 文件类型
     */
    String type;

    /**
     * 创建时间
     */
    Date createTime;
}
