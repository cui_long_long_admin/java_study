package com.cuicui.demo.vo;

import lombok.Data;

@Data
public class LoginVO {
    /**
     * 用户名
     */
    String userName;

    /**
     * 密码
     */
    String passWrod;
}
