package com.cuicui.demo.aspect;

import cn.hutool.extra.servlet.JakartaServletUtil;
import com.cuicui.demo.annotation.LogRecorder;
import com.cuicui.demo.dao.LogDataMapper;
import com.cuicui.demo.entity.LogData;
import com.cuicui.demo.utils.NetworkUtil;
import com.cuicui.demo.vo.UserVO;
import jakarta.servlet.http.HttpServletRequest;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.ServletRequestAttributes;

import java.lang.reflect.Method;
import java.util.Date;
import java.util.Objects;

@Aspect
@Component
public class LoggerAspect {
    @Autowired
    private LogDataMapper logDataMapper;

    @Pointcut(value = "execution(* com.cuicui.demo.controller..*(..))")
    public void logPointcut() {}

    @Before("logPointcut()")
    public void beforeLogger (JoinPoint joinPoint) {
        // 跳过未登录用户、不记录非必要数据
//        if(SecurityContextHolder.getContext().getAuthentication()==null){
//            return;
//        }
//
//        // 获取当前登录用户信息
//        UserVO user = (UserVO) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        // 获取用户请求相关信息
        ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        HttpServletRequest request = Objects.requireNonNull(attributes).getRequest();

        // 解析请求信息
        String ip = NetworkUtil.getClientIPByHeader(request);
        String requestUrl = request.getRequestURI().toString();
        String userAgent = JakartaServletUtil.getHeaderIgnoreCase(request, "User-Agent");

        Method signature = ((MethodSignature) joinPoint.getSignature()).getMethod();
        LogRecorder logRecorder = signature.getAnnotation(LogRecorder.class);

        if(logRecorder != null){
            LogData logData = new LogData();
            Date now = new Date();
            logData.setCreateTime(now);
            logData.setIp(ip);
//            logData.setCreateName(user.getName());
            logData.setUserAgent(userAgent);
            logData.setContent(logRecorder.operation());
            logData.setRequestedurl(requestUrl);
            logDataMapper.insert(logData);
        }
    }
}
