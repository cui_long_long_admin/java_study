package com.cuicui.demo.dao;

import com.cuicui.demo.entity.AppendixData;
import com.cuicui.demo.entity.AppendixDataExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface AppendixDataMapper {
    long countByExample(AppendixDataExample example);

    int deleteByExample(AppendixDataExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(AppendixData row);

    int insertSelective(AppendixData row);

    List<AppendixData> selectByExample(AppendixDataExample example);

    AppendixData selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("row") AppendixData row, @Param("example") AppendixDataExample example);

    int updateByExample(@Param("row") AppendixData row, @Param("example") AppendixDataExample example);

    int updateByPrimaryKeySelective(AppendixData row);

    int updateByPrimaryKey(AppendixData row);
}