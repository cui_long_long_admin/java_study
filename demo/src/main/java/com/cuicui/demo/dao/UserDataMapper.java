package com.cuicui.demo.dao;

import com.cuicui.demo.entity.UserData;
import com.cuicui.demo.entity.UserDataExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface UserDataMapper {
    long countByExample(UserDataExample example);

    int deleteByExample(UserDataExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(UserData row);

    int insertSelective(UserData row);

    List<UserData> selectByExample(UserDataExample example);

    UserData selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("row") UserData row, @Param("example") UserDataExample example);

    int updateByExample(@Param("row") UserData row, @Param("example") UserDataExample example);

    int updateByPrimaryKeySelective(UserData row);

    int updateByPrimaryKey(UserData row);
}