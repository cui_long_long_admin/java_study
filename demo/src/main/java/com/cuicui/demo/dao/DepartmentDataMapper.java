package com.cuicui.demo.dao;

import com.cuicui.demo.entity.DepartmentData;
import com.cuicui.demo.entity.DepartmentDataExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface DepartmentDataMapper {
    long countByExample(DepartmentDataExample example);

    int deleteByExample(DepartmentDataExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(DepartmentData row);

    int insertSelective(DepartmentData row);

    List<DepartmentData> selectByExample(DepartmentDataExample example);

    DepartmentData selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("row") DepartmentData row, @Param("example") DepartmentDataExample example);

    int updateByExample(@Param("row") DepartmentData row, @Param("example") DepartmentDataExample example);

    int updateByPrimaryKeySelective(DepartmentData row);

    int updateByPrimaryKey(DepartmentData row);
}