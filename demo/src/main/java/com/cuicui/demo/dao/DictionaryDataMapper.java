package com.cuicui.demo.dao;

import com.cuicui.demo.entity.DictionaryData;
import com.cuicui.demo.entity.DictionaryDataExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface DictionaryDataMapper {
    long countByExample(DictionaryDataExample example);

    int deleteByExample(DictionaryDataExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(DictionaryData row);

    int insertSelective(DictionaryData row);

    List<DictionaryData> selectByExample(DictionaryDataExample example);

    DictionaryData selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("row") DictionaryData row, @Param("example") DictionaryDataExample example);

    int updateByExample(@Param("row") DictionaryData row, @Param("example") DictionaryDataExample example);

    int updateByPrimaryKeySelective(DictionaryData row);

    int updateByPrimaryKey(DictionaryData row);
}