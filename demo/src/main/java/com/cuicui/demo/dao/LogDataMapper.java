package com.cuicui.demo.dao;

import com.cuicui.demo.entity.LogData;
import com.cuicui.demo.entity.LogDataExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface LogDataMapper {
    long countByExample(LogDataExample example);

    int deleteByExample(LogDataExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(LogData row);

    int insertSelective(LogData row);

    List<LogData> selectByExample(LogDataExample example);

    LogData selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("row") LogData row, @Param("example") LogDataExample example);

    int updateByExample(@Param("row") LogData row, @Param("example") LogDataExample example);

    int updateByPrimaryKeySelective(LogData row);

    int updateByPrimaryKey(LogData row);
}