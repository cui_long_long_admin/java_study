package com.cuicui.demo.dao;

import com.cuicui.demo.entity.MenuData;
import com.cuicui.demo.entity.MenuDataExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface MenuDataMapper {
    long countByExample(MenuDataExample example);

    int deleteByExample(MenuDataExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(MenuData row);

    int insertSelective(MenuData row);

    List<MenuData> selectByExample(MenuDataExample example);

    MenuData selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("row") MenuData row, @Param("example") MenuDataExample example);

    int updateByExample(@Param("row") MenuData row, @Param("example") MenuDataExample example);

    int updateByPrimaryKeySelective(MenuData row);

    int updateByPrimaryKey(MenuData row);
}