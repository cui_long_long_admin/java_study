package com.cuicui.demo.dao;

import com.cuicui.demo.entity.RoleData;
import com.cuicui.demo.entity.RoleDataExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface RoleDataMapper {
    long countByExample(RoleDataExample example);

    int deleteByExample(RoleDataExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(RoleData row);

    int insertSelective(RoleData row);

    List<RoleData> selectByExample(RoleDataExample example);

    RoleData selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("row") RoleData row, @Param("example") RoleDataExample example);

    int updateByExample(@Param("row") RoleData row, @Param("example") RoleDataExample example);

    int updateByPrimaryKeySelective(RoleData row);

    int updateByPrimaryKey(RoleData row);
}