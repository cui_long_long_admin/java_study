package com.cuicui.demo.dao;

import com.cuicui.demo.entity.DictionaryValueData;
import com.cuicui.demo.entity.DictionaryValueDataExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface DictionaryValueDataMapper {
    long countByExample(DictionaryValueDataExample example);

    int deleteByExample(DictionaryValueDataExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(DictionaryValueData row);

    int insertSelective(DictionaryValueData row);

    List<DictionaryValueData> selectByExample(DictionaryValueDataExample example);

    DictionaryValueData selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("row") DictionaryValueData row, @Param("example") DictionaryValueDataExample example);

    int updateByExample(@Param("row") DictionaryValueData row, @Param("example") DictionaryValueDataExample example);

    int updateByPrimaryKeySelective(DictionaryValueData row);

    int updateByPrimaryKey(DictionaryValueData row);
}