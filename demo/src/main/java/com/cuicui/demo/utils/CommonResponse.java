package com.cuicui.demo.utils;

import com.cuicui.demo.enumCommon.BaseErrorInfo;
import com.cuicui.demo.enumCommon.ExceptionEnum;
import lombok.Data;

@Data
public class
CommonResponse<T> {
    /**
     * 响应代码
     */
    private String code;

    /**
     * 相应信息
     */
    private String message;

    /**
     * 相应结果
     */
    private T result;

    public CommonResponse () {
    }

    public CommonResponse (BaseErrorInfo baseErrorInfo) {
        this.code = baseErrorInfo.getErrorCode();
        this.message = baseErrorInfo.getErrorMessage();
    }

    public static CommonResponse success () {
        return success(null);
    }

    /**
     * 成功
     *
     * @param data
     * @return
     */
    public static CommonResponse success(Object data) {
        CommonResponse rb = new CommonResponse();
        rb.setCode(ExceptionEnum.SUCCESS.getErrorCode());
        rb.setMessage(ExceptionEnum.SUCCESS.getErrorMessage());
        rb.setResult(data);
        return rb;
    }


    public static CommonResponse error(String code, String message) {
        CommonResponse rb = new CommonResponse();
        rb.setCode(code);
        rb.setMessage(message);
        rb.setResult(null);
        return rb;
    }

    public static CommonResponse front_error(String str){
        CommonResponse rb = new CommonResponse();
        rb.setCode(ExceptionEnum.PARAMS_ERROR.getErrorCode());
        rb.setMessage(str);
        rb.setResult(null);
        return rb;
    }

    public static CommonResponse front_error(){
        CommonResponse rb = new CommonResponse();
        rb.setCode(ExceptionEnum.PARAMS_ERROR.getErrorCode());
        rb.setMessage(ExceptionEnum.PARAMS_ERROR.getErrorMessage());
        rb.setResult(null);
        return rb;
    }

    public static CommonResponse system_error(){
        CommonResponse rb = new CommonResponse();
        rb.setCode(ExceptionEnum.SYSTEM_ERROR.getErrorCode());
        rb.setMessage(ExceptionEnum.SYSTEM_ERROR.getErrorMessage());
        rb.setResult(null);
        return rb;
    }

}
