package com.cuicui.demo.service;

import com.cuicui.demo.dao.DepartmentDataMapper;
import com.cuicui.demo.dao.UserDataMapper;
import com.cuicui.demo.entity.DepartmentData;
import com.cuicui.demo.entity.DepartmentDataExample;
import com.cuicui.demo.entity.UserData;
import com.cuicui.demo.entity.UserDataExample;
import com.cuicui.demo.vo.DepartmentVO;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.apache.catalina.User;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class DepartmentService {
    @Autowired
    private DepartmentDataMapper departmentDao;

    @Autowired
    private UserDataMapper userDao;

    public List<DepartmentVO> getList (String name) {
        DepartmentDataExample selectDepartmentExample = new DepartmentDataExample();
        DepartmentDataExample.Criteria criteria = selectDepartmentExample.createCriteria();
        List<DepartmentVO> departmentVOList = null;
        if(name != null){
            criteria.andNameLike(name);
        }
        criteria.andIsdealtedEqualTo(0);
        List<DepartmentData> departmentData = departmentDao.selectByExample(selectDepartmentExample);
        departmentVOList = departmentData.stream().map(DepartmentVO::new).collect(Collectors.toList());
        return departmentVOList;
    }

    public PageInfo<DepartmentVO> getPage(String name,Integer pageNo, Integer pageSize) {
        DepartmentDataExample selectDepartmentExample = new DepartmentDataExample();
        PageHelper.startPage(pageNo, pageSize);
        DepartmentDataExample.Criteria criteria = selectDepartmentExample.createCriteria();
        List<DepartmentVO> departmentVOList = null;
        if(name != null){
            criteria.andNameLike(name);
        }
        List<DepartmentData> departmentData = departmentDao.selectByExample(selectDepartmentExample);
        departmentVOList = departmentData.stream().map(DepartmentVO::new).collect(Collectors.toList());
        PageInfo<DepartmentVO> pageInfo = new PageInfo<>();
        BeanUtils.copyProperties(departmentVOList, pageInfo);
        pageInfo.setList(departmentVOList);
        return pageInfo;
    }

    public Integer addDepartment (DepartmentVO departmentVO) {
        DepartmentData departmentData = new DepartmentData();
        BeanUtils.copyProperties(departmentVO, departmentData);
        departmentData.setCreateTime(new Date());
        departmentData.setIsdealted(0);
        departmentDao.insertSelective(departmentData);
        return departmentData.getId();
    }

    public Boolean remove(Integer id) {
        DepartmentData departmentData = new DepartmentData();
        departmentData.setIsdealted(1);
        departmentData.setId(id);
        // 查询是否存在子部门
        DepartmentDataExample departmentDataExample = new DepartmentDataExample();
        DepartmentDataExample.Criteria criteria = departmentDataExample.createCriteria();
        // 查询用户
        UserDataExample userDataExample = new UserDataExample();
        UserDataExample.Criteria userCriteria = userDataExample.createCriteria();
        if(id != null){
            criteria.andParentIdEqualTo(id);
            userCriteria.andDepartmentIdEqualTo(id);
            List<DepartmentData> departmentDataList = departmentDao.selectByExample(departmentDataExample);
            List<UserData> userDataList = userDao.selectByExample(userDataExample);
            // 存在下级部门不可删除
            if(!departmentDataList.isEmpty()){
                throw new Error("当前部门下存在子部门、不可删除!");
            }
            // 存在下级用户不可删除
            if(!userDataList.isEmpty()){
                throw new Error("当前部门下存在用户、不可删除!");
            }
        }
        try{
            departmentDao.updateByPrimaryKeySelective(departmentData);;
        }catch (Exception e){
            return false;
        }
        return true;
    }

    public Boolean update (DepartmentVO departmentVO) {
        DepartmentData departmentData = new DepartmentData();
        BeanUtils.copyProperties(departmentVO, departmentData);
        DepartmentDataExample departmentDataExample = new DepartmentDataExample();
        DepartmentDataExample.Criteria criteria = departmentDataExample.createCriteria();
        if(departmentVO.getId() == null){
            throw new Error("部门ID必填！");
        }
        criteria.andIdEqualTo(departmentVO.getId());
        List<DepartmentData> departmentDataList = departmentDao.selectByExample(departmentDataExample);
        if(departmentDataList.isEmpty() || departmentDataList.get(0).getIsdealted() == 1){
            throw new Error("当前部门不存在");
        }
        try{
            departmentDao.updateByPrimaryKeySelective(departmentData);
        }catch (Exception e){
            return false;
        }
        return true;
    }
}
