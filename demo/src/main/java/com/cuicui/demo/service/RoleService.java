package com.cuicui.demo.service;

import com.cuicui.demo.dao.RoleDataMapper;
import com.cuicui.demo.entity.RoleData;
import com.cuicui.demo.entity.RoleDataExample;
import com.cuicui.demo.utils.CommonResponse;
import com.cuicui.demo.vo.RoleVO;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RoleService {

    @Autowired
    private RoleDataMapper roleDataDao;

    public CommonResponse getList (String name) {
        RoleDataExample roleDataExample = new RoleDataExample();
        RoleDataExample.Criteria criteria = roleDataExample.createCriteria();
        if(name.equals("") && name != null){
            criteria.andNameLike("%"+ name + "%");
        }
        List<RoleData> roleDataList = roleDataDao.selectByExample(roleDataExample);
        if(roleDataList.isEmpty() || roleDataList.size() == 0){
            return null;
        }
        List<RoleVO> roleVOList = roleDataList.stream().map(roleItem->{
          RoleVO roleVO = new RoleVO();
          BeanUtils.copyProperties(roleItem, roleVO);
          return  roleVO;
        }).toList();

        return  CommonResponse.success(roleVOList);
    }

    public CommonResponse getPage (String name, Integer pageNo, Integer pageSize) {
        RoleDataExample roleDataExample = new RoleDataExample();
        RoleDataExample.Criteria criteria = roleDataExample.createCriteria();
        if(name!= null && name.equals("")){
            criteria.andNameLike("%"+ name + "%");
        }
        PageHelper.startPage(pageNo, pageSize);
        List<RoleData> roleDataList = roleDataDao.selectByExample(roleDataExample);
        List<RoleVO> roleVOList = roleDataList.stream().map(roleItem->{
            RoleVO roleVO = new RoleVO();
            BeanUtils.copyProperties(roleItem, roleVO);
            return  roleVO;
        }).toList();
        PageInfo pageInfo = new PageInfo<>();
        BeanUtils.copyProperties(roleVOList, pageInfo);
        pageInfo.setList(roleVOList);
        return CommonResponse.success(pageInfo);
    }

    public CommonResponse<Boolean> add (RoleVO roleVO) {
        RoleData roleData = new RoleData();
        BeanUtils.copyProperties(roleVO, roleData);
        try{
            roleDataDao.insert(roleData);
        }catch (Exception e){
            return  CommonResponse.error("500", "添加失败");
        }
        return CommonResponse.success(true);
    }

    public CommonResponse remove (Integer id) {
        try{
            roleDataDao.deleteByPrimaryKey(id);
        }catch (Exception e){
            return CommonResponse.error("500", "删除失败");
        }
        return CommonResponse.success(true);
    }

    public CommonResponse update (RoleVO roleVO) {
        RoleData roleData = new RoleData();
        BeanUtils.copyProperties(roleVO, roleData);
        try{
            roleDataDao.updateByPrimaryKey(roleData);
        }catch (Exception e){
            return CommonResponse.error("500", "更新失败");
        }
        return CommonResponse.success(true);
    }
}
