package com.cuicui.demo.service;

import com.cuicui.demo.dao.AppendixDataMapper;
import com.cuicui.demo.entity.AppendixData;
import com.cuicui.demo.vo.AppendixVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

@Service
public class AppendixService {
    @Autowired
    private AppendixDataMapper AppendixDao;
    // 文件上传目标位置
    private static final String BASE_DIR = "D:\\phpstudy_pro\\WWW\\demo_cui_files\\";

    public AppendixVO upload (MultipartFile file){
        // 获取文件名成
        String fileName = file.getOriginalFilename();
        Date date = new Date();
        SimpleDateFormat formatDate = new SimpleDateFormat("yyyy-MM-dd");
        String resultDate = formatDate.format(date);
        AppendixVO resultVo = new AppendixVO();
        try{
            // 新建一个文件路径
            resultVo.setName(fileName);
            File uploadFile = new File(BASE_DIR + resultDate + "\\file_"  + fileName);
            // 当父目录不存在时时、自动创建
            if(!uploadFile.getParentFile().exists()){
                uploadFile.getParentFile().mkdirs();
            }
            // 存储文件到磁盘
            file.transferTo(uploadFile);
            // 写入附件表
            if (uploadFile.exists() && uploadFile.length() > 0){
                AppendixData appendixData = new AppendixData();
                appendixData.setPath(uploadFile.getPath());
                appendixData.setCreateTime(date);
                resultVo.setPath(uploadFile.getPath());
                resultVo.setCreateTime(date);
                AppendixDao.insertSelective(appendixData);
            }
        }catch (IOException e){
            System.out.println(e.toString());
            e.printStackTrace();
        }

        return resultVo;
    }
}
