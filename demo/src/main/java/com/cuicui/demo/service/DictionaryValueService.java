package com.cuicui.demo.service;

import com.cuicui.demo.dao.DictionaryDataMapper;
import com.cuicui.demo.dao.DictionaryValueDataMapper;
import com.cuicui.demo.entity.DictionaryData;
import com.cuicui.demo.entity.DictionaryDataExample;
import com.cuicui.demo.entity.DictionaryValueData;
import com.cuicui.demo.entity.DictionaryValueDataExample;
import com.cuicui.demo.utils.CommonResponse;
import com.cuicui.demo.vo.DictionaryValueVO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class DictionaryValueService {
    @Autowired
    private DictionaryDataMapper dictionaryDataMapper;

    @Autowired
    private DictionaryValueDataMapper dictionaryValueDataDao;

    public CommonResponse addValue(DictionaryValueVO dictionaryValueVO){
        DictionaryValueData dictionaryValueData = new DictionaryValueData();
        BeanUtils.copyProperties(dictionaryValueVO, dictionaryValueData);
        try{
            dictionaryValueDataDao.insertSelective(dictionaryValueData);
        }catch (Exception e){
            System.out.println(e.toString() + "==============================");
            return CommonResponse.error("500", "新增失败");
        }
        return CommonResponse.success(dictionaryValueData.getId());
    }

    public CommonResponse getDictionaryValue(Integer id, String code){
        if(code == null && id == null){
            return CommonResponse.error("400", "参数错误");
        }
        // 字典
        DictionaryDataExample dictionaryDataExample = new DictionaryDataExample();
        DictionaryDataExample.Criteria dictionaryDataCriteria = dictionaryDataExample.createCriteria();
        if(code != null && !code.equals("")){
            dictionaryDataCriteria.andCodeEqualTo(code);
            List<DictionaryData> dictionaryDataList = dictionaryDataMapper.selectByExample(dictionaryDataExample);
            if(!dictionaryDataList.isEmpty()){
                id = dictionaryDataList.get(0).getId();
            }
        }
        // 字典值
        DictionaryValueDataExample dictionaryValueDataExample = new DictionaryValueDataExample();
        DictionaryValueDataExample.Criteria criteria = dictionaryValueDataExample.createCriteria();
        List<DictionaryValueData> dictionaryValueDataList = null;
        if(id != null){
            criteria.andDictionaryIdEqualTo(id);
            dictionaryValueDataList = dictionaryValueDataDao.selectByExample(dictionaryValueDataExample);
        }
        if(dictionaryValueDataList.isEmpty()){
            return CommonResponse.success(null);
        }
        return CommonResponse.success(dictionaryValueDataList.stream().map(dictionaryValueData -> {
            DictionaryValueVO dictionaryValueVO = new DictionaryValueVO();
            BeanUtils.copyProperties(dictionaryValueData, dictionaryValueVO);
            return dictionaryValueVO;
        }).collect(Collectors.toList()));
    }
}
