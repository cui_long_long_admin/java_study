package com.cuicui.demo.service;

import com.cuicui.demo.dao.DictionaryDataMapper;
import com.cuicui.demo.dao.DictionaryValueDataMapper;
import com.cuicui.demo.entity.DictionaryData;
import com.cuicui.demo.entity.DictionaryDataExample;
import com.cuicui.demo.entity.DictionaryValueData;
import com.cuicui.demo.utils.CommonResponse;
import com.cuicui.demo.vo.DictionaryVO;
import com.cuicui.demo.vo.DictionaryValueVO;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class DictionaryService {
    @Autowired
    private DictionaryDataMapper dictionaryDataDao;

    @Autowired
    private DictionaryValueDataMapper dictionaryValueDataDao;

    public CommonResponse<DictionaryVO> getList (){
        List<DictionaryData> dictionaryDataList = dictionaryDataDao.selectByExample(null);
        List<DictionaryVO> dictionaryVOList = dictionaryDataList.stream().map((dictionaryDataItem)->{
                DictionaryVO dictionaryVO = new DictionaryVO();
                BeanUtils.copyProperties(dictionaryDataItem, dictionaryVO);
                return dictionaryVO;
        }).collect(Collectors.toList());
        if(dictionaryVOList.isEmpty()){
            return CommonResponse.success(null);
        }
        return CommonResponse.success(dictionaryVOList);
    }
    /**
     * 分页查询字典数据
     *
     * @param name 字典名称
     * @param pageNo 当前页码
     * @param pageSize 每页显示条数
     * @return 查询结果
     */
    public CommonResponse getPage (String name, Integer pageNo, Integer pageSize){
        DictionaryDataExample selectDictionaryExample = new DictionaryDataExample();
        DictionaryDataExample.Criteria criteria = selectDictionaryExample.createCriteria();
        PageHelper.startPage(pageNo, pageSize);
        if(name != null){
            criteria.andNameLike(name);
        }
        List<DictionaryData> dictionaryDataList = dictionaryDataDao.selectByExample(selectDictionaryExample);
        List<DictionaryVO> dictionaryVOList = dictionaryDataList.stream().map((dictionaryDataItem)->{
            DictionaryVO dictionaryVO = new DictionaryVO();
            BeanUtils.copyProperties(dictionaryDataItem, dictionaryVO);
            return dictionaryVO;
        }).collect(Collectors.toList());
        PageInfo<DictionaryVO> pageInfo = new PageInfo<>();
        pageInfo.setList(dictionaryVOList);

        return CommonResponse.success(pageInfo);
    }

    public CommonResponse add (DictionaryVO dictionaryVO) {
        DictionaryData dictionaryData = new DictionaryData();
        BeanUtils.copyProperties(dictionaryVO, dictionaryData);
        try{
            dictionaryDataDao.insertSelective(dictionaryData);
        }catch (Exception e){
            throw new Error(e);
        }
        return CommonResponse.success(dictionaryData.getId());
    }

    public CommonResponse remove (Integer id) {
        try{
            dictionaryDataDao.deleteByPrimaryKey(id);
        }catch (Exception e){
            return CommonResponse.error("500", "系统繁忙");
        }
        return CommonResponse.success(null);
    }

    public CommonResponse removeValue (Integer id) {
        try{
            dictionaryValueDataDao.deleteByPrimaryKey(id);
        }catch (Exception e){
            return CommonResponse.error("500", "系统繁忙");
        }
        return CommonResponse.success(null);
    }

    public CommonResponse update (DictionaryVO dictionaryVO) {
        DictionaryData dictionaryData = new DictionaryData();
        BeanUtils.copyProperties(dictionaryVO, dictionaryData);
        try{
            dictionaryDataDao.updateByPrimaryKey(dictionaryData);
        }catch (Exception e){
            return CommonResponse.error("500", "系统繁忙");
        }
        return CommonResponse.success(true);
    }

    public CommonResponse updateValue (DictionaryValueVO dictionaryValueVO) {
        DictionaryValueData dictionaryValueData = new DictionaryValueData();
        BeanUtils.copyProperties(dictionaryValueVO, dictionaryValueData);
        try{
            dictionaryValueDataDao.updateByPrimaryKey(dictionaryValueData);
        }catch (Exception e){
            return CommonResponse.error("500", "系统繁忙");
        }
        return CommonResponse.success(true);
    }

}
