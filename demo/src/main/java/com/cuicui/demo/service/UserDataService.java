package com.cuicui.demo.service;

import com.auth0.jwt.interfaces.Claim;
import com.cuicui.demo.dao.UserDataMapper;
import com.cuicui.demo.entity.UserData;
import com.cuicui.demo.entity.UserDataExample;
import com.cuicui.demo.utils.JwtUtils;
import com.cuicui.demo.utils.MD5;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.cuicui.demo.vo.UserVO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


@Service
public class UserDataService {
    @Autowired
    private UserDataMapper userDao;

    /**
     *
     * @param name 用户名
     * @param page 分页
     * @param pageSize 分页尺寸
     * @return PageInfo<UserVO>
     */
    public PageInfo<UserVO> getPage(String name, Integer page, Integer pageSize, Integer departmentId) {
        UserDataExample userExampleData = new UserDataExample();
        //利用User标准实体类设置查询参数
        UserDataExample.Criteria criteria = userExampleData.createCriteria();
        // 名称查询
        if(name != null && !name.equals("")){
            criteria.andNameLike("%"+name+"%");
        }
        // 部门id查询
        if(departmentId != null){
            criteria.andDepartmentIdEqualTo(departmentId);
        }
        //设置查询未假删除数据
        criteria.andIsDeletedEqualTo(0);
        //设置分页数量
        PageHelper.startPage(page, pageSize);
        //查询数据结果
        List<UserData> userDataList = userDao.selectByExample(userExampleData);
        //根据io层展示给前端看的结果进行规范
        List<UserVO> users = userDataList.stream().map(user -> {
            UserVO userItem = new UserVO(user);
            userItem.setPwd(null);
            return userItem;
        }).collect(Collectors.toList());
        //放入到pageInfo容器包裹的数据中
        PageInfo<UserVO> userPageInfo = new PageInfo<>();
        BeanUtils.copyProperties(users, userPageInfo);
        userPageInfo.setList(users);
        return userPageInfo;
    }

    public Integer Add (UserVO addParams){
        UserData userdata = new UserData();
        UserDataExample userDataExample = new UserDataExample();
        UserDataExample.Criteria criteria = userDataExample.createCriteria();
        criteria.andNameEqualTo(addParams.getName());
        List<UserData> userDataList = userDao.selectByExample(userDataExample);
        if(!userDataList.isEmpty() && userDataList.size() > 0){
           return -1;
        }
        MD5 md5 = new MD5();
        Integer userId;
        addParams.setIsDeleted(0);
        addParams.setCreateTime(new Date());
        addParams.setPwd(md5.encode(addParams.getPwd()));
        BeanUtils.copyProperties(addParams, userdata);
        try{
           userDao.insertSelective(userdata);
        }catch (Error err){
            throw new Error(err);
        }
        userId = userdata.getId();
        return userId;
    }

    public Boolean remove (Integer id) {
        try{
            userDao.deleteByPrimaryKey(id);
        }catch (Error err){
            throw new Error(err);
        }
        return true;
    }

    public Boolean del (Integer id) {
        UserData userData = userDao.selectByPrimaryKey(id);
        if(userData.getIsDeleted() == 1){
            throw new Error("当前用户不存在");
        }
        userData.setIsDeleted(1);
        userDao.updateByPrimaryKeySelective(userData);
        return  true;
    }

    public Boolean update (UserVO userInfo) {
        Integer id = userInfo.getId();
        if(id == null){
            throw new Error("当前用户不存在");
        }
        UserData userData = userDao.selectByPrimaryKey(id);
        userInfo.setPwd(userData.getPwd());
        userInfo.setIsDeleted(userData.getIsDeleted());
        BeanUtils.copyProperties(userInfo, userData);
        userDao.updateByPrimaryKeySelective(userData);
        return true;
    }

    public UserVO getData (String token) {
        Map<String, Claim> userData = JwtUtils.getTokenInfo(token);
        UserDataExample userDataExample = new UserDataExample();
        UserDataExample.Criteria criteria = userDataExample.createCriteria();
        userData.forEach((key, value)->{
            if(key.equals("loginName")){
                criteria.andNameEqualTo(value.asString());
            }
        });
        List<UserData> userDataList = userDao.selectByExample(userDataExample);
        List<UserVO> userVoList;
        if(userDataList != null && userDataList.size() == 1){
            userVoList = userDataList.stream().map((item)->{
                UserVO userItem = new UserVO(item);
                userItem.setPwd(null);
                return userItem;
            }).toList();
        }else{
            return new UserVO();
        }
        return userVoList.get(0);
    }

    public UserVO getDetail (Integer id){
        UserData userData = userDao.selectByPrimaryKey(id);
        userData.setPwd(null);
        return new UserVO(userData);
    }
}
