/*
 Navicat Premium Data Transfer

 Source Server         : local
 Source Server Type    : MySQL
 Source Server Version : 50726
 Source Host           : localhost:3306
 Source Schema         : cui_demo

 Target Server Type    : MySQL
 Target Server Version : 50726
 File Encoding         : 65001

 Date: 04/12/2024 21:58:54
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for appendix_data
-- ----------------------------
DROP TABLE IF EXISTS `appendix_data`;
CREATE TABLE `appendix_data`  (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `path` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '文件路径',
  `create_time` datetime NULL DEFAULT NULL,
  `type` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '文件类型',
  `size` int(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 5 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of appendix_data
-- ----------------------------
INSERT INTO `appendix_data` VALUES (4, NULL, 'D:\\phpstudy_pro\\WWW\\demo_cui_files\\2024-11-13\\file_512x512bb.jpg', '2024-11-13 12:04:07', NULL, NULL);

-- ----------------------------
-- Table structure for department_data
-- ----------------------------
DROP TABLE IF EXISTS `department_data`;
CREATE TABLE `department_data`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `parent_id` int(11) NOT NULL,
  `desc` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '部门描述',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `isDealted` int(1) UNSIGNED ZEROFILL NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 6 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of department_data
-- ----------------------------
INSERT INTO `department_data` VALUES (1, '总公司', 0, '总公司', '2024-11-15 11:59:21', 0);
INSERT INTO `department_data` VALUES (2, '分公司', 1, 'string', '2024-11-15 15:57:50', 1);
INSERT INTO `department_data` VALUES (3, '某某牛逼公司', 1, '某某牛逼公司', '2024-11-15 16:00:29', 0);
INSERT INTO `department_data` VALUES (4, 'cuicui部门', 1, '222222222', '2024-12-02 11:33:37', 0);
INSERT INTO `department_data` VALUES (5, 'cuicui部门23', 4, '脆脆部门23', '2024-12-02 11:37:48', 1);

-- ----------------------------
-- Table structure for dictionary_data
-- ----------------------------
DROP TABLE IF EXISTS `dictionary_data`;
CREATE TABLE `dictionary_data`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `code` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 4 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of dictionary_data
-- ----------------------------
INSERT INTO `dictionary_data` VALUES (2, '性别', 'gender');

-- ----------------------------
-- Table structure for dictionary_value_data
-- ----------------------------
DROP TABLE IF EXISTS `dictionary_value_data`;
CREATE TABLE `dictionary_value_data`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `value` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `dictionary_id` int(11) NOT NULL,
  `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `name_dictionaryId`(`name`, `dictionary_id`) USING BTREE,
  INDEX `dictionary_id`(`dictionary_id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 4 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of dictionary_value_data
-- ----------------------------
INSERT INTO `dictionary_value_data` VALUES (1, 'male', '男', 2, '男');
INSERT INTO `dictionary_value_data` VALUES (2, 'female', '女', 2, '女');

-- ----------------------------
-- Table structure for log_data
-- ----------------------------
DROP TABLE IF EXISTS `log_data`;
CREATE TABLE `log_data`  (
  `id` int(11) NOT NULL,
  `create_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `create_time` datetime NOT NULL,
  `content` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `type` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of log_data
-- ----------------------------

-- ----------------------------
-- Table structure for menu_data
-- ----------------------------
DROP TABLE IF EXISTS `menu_data`;
CREATE TABLE `menu_data`  (
  `id` int(1) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `type` int(11) NOT NULL COMMENT '0:菜单；1:界面',
  `path` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `needPermission` int(11) NOT NULL COMMENT '是否需要权限',
  `icon` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of menu_data
-- ----------------------------

-- ----------------------------
-- Table structure for role_data
-- ----------------------------
DROP TABLE IF EXISTS `role_data`;
CREATE TABLE `role_data`  (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `desc` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 4 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of role_data
-- ----------------------------
INSERT INTO `role_data` VALUES (1, '测试', '测试人员');
INSERT INTO `role_data` VALUES (2, '管理员', '超管');

-- ----------------------------
-- Table structure for user_data
-- ----------------------------
DROP TABLE IF EXISTS `user_data`;
CREATE TABLE `user_data`  (
  `id` int(32) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `phone` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `gender` int(1) UNSIGNED ZEROFILL NOT NULL COMMENT '0:未知1:男;2女',
  `pwd` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `is_deleted` int(1) UNSIGNED ZEROFILL NOT NULL,
  `create_time` datetime NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '入职时间',
  `avatarId` int(11) NULL DEFAULT NULL COMMENT '附件头像',
  `department_id` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 152 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of user_data
-- ----------------------------
INSERT INTO `user_data` VALUES (142, 'cuicui', NULL, 2, 'E10ADC3949BA59ABBE56E057F20F883E', 0, '2024-11-20 14:47:17', NULL, 3);
INSERT INTO `user_data` VALUES (143, 'cuicui', NULL, 0, 'E10ADC3949BA59ABBE56E057F20F883E', 1, '2024-11-20 14:47:18', NULL, 3);
INSERT INTO `user_data` VALUES (145, 'string', NULL, 0, 'B45CFFE084DD3D20D928BEE85E7B0F21', 0, '2024-11-20 14:47:18', NULL, 3);
INSERT INTO `user_data` VALUES (151, 'zhangsanfeng', NULL, 1, 'E10ADC3949BA59ABBE56E057F20F883E', 0, '2024-11-20 14:47:19', NULL, 3);
INSERT INTO `user_data` VALUES (150, 'zhangsan', NULL, 0, 'E10ADC3949BA59ABBE56E057F20F883E', 0, '2024-11-20 14:47:19', NULL, 1);
INSERT INTO `user_data` VALUES (149, 'string1', NULL, 0, 'B45CFFE084DD3D20D928BEE85E7B0F21', 0, '2024-11-20 14:47:20', NULL, 1);

SET FOREIGN_KEY_CHECKS = 1;
